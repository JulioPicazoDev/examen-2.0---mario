package com.softtek.academy.javaweb.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import com.softtek.academy.javaweb.beans.Person;

@Repository("personRepository")
public class PersonRepository implements PersonDAO {
	@PersistenceContext
    private EntityManager entityManager;
    @Override
    public List<Person> getAll() {
        return (List<Person>) entityManager.createQuery("SELECT p FROM Student p", Person.class).getResultList();
    }
    @Override
    public Person getById(int id) {
        return entityManager.find(Person.class, id);
    }
    @Override
    public void delete(int id) {
        Person person = entityManager.find(Person.class, id);
        entityManager.remove(person);
    }
    @Override
    public void update(int id, Person person) {
        Person per = entityManager.find(Person.class, id);
        per.setName(person.getName());
        per.setAge(person.getAge());
    }
    @Override
    public void addUser(Person person) {
        entityManager.persist(person);
    }

}
