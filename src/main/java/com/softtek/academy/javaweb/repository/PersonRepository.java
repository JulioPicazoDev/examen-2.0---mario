package com.softtek.academy.javaweb.repository;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.softtek.academy.javaweb.beans.Person;


public class PersonRepository {
	
	//@Inject @MySQLDatabase
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<Person> getAllStdents() {
		Query query = entityManager.createQuery("SELECT b from Student b");
		return (List<Person>) query.getResultList();
	}
	
	public Person getById(long id) {
		Person p = entityManager.find(Person.class, id);
		return p;
	}
	
}
