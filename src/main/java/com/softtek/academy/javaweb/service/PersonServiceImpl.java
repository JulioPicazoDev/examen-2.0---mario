package com.softtek.academy.javaweb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.javaweb.beans.Person;
import com.softtek.academy.javaweb.dao.PersonDAO;


@Service

public class PersonServiceImpl implements PersonService {
	 @Autowired
	    @Qualifier("studentRepository")
	    private PersonDAO personDAO;
	    @Override
	    public List<Person> getAll() {
	        return personDAO.getAll();
	    }
	    @Override
	    public Person getById(int id) {
	        return personDAO.getById(id);
	    }
	    @Override
	    @Transactional
	    public void delete(int id) {
	    	personDAO.delete(id);
	    }
	    @Override
	    @Transactional
	    public void update(int id, Person person) {
	    	personDAO.update(id, person);
	    }
	    @Override
	    @Transactional
	    public void addUser(Person person) {
	    	personDAO.addUser(person);
	    }
}
