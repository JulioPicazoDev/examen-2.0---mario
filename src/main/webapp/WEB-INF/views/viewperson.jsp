<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head><meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
table, th, td {
  border: 1px solid black;
}
</style>
</head>
<body>
<table style="width:100%">
  <tr>
    <th>To Do</th>
    <th>Done</th>
    <th>Action</th>
  
  </tr>
        <tbody>
            <c:forEach items="${list}" var="person">
            <tr>
                <td><a href="/SpringMVCApp/personUpdateForm/${person.getId()}">${person.getId()}</a></td>
                <td>${person.getName()}</td>
                <td>${person.getAge()}</td>
                <td><a href="/SpringMVCApp/delete/${person.getId()}">Dar de baja</a></td>
            </tr>
            </c:forEach>
        </tbody>

  
</table>
<a href="./index.jsp">Go To Menu</a>
</body>
</html>